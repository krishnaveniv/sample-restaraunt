$(document).ready(function () {
    $("#getCity").on("click", function () {
        citySelection();
    });
    //query for City selection-----------------
    function citySelection() {
        var dropdown = $('#select_id').val();
        var settings = {
            "url": "https://developers.zomato.com/api/v2.1/search?entity_id=" + dropdown +
                "&entity_type=city" + "&count=100",
            "method": "GET",
            "headers": {
                "user-key": "fb45167d1e80110706827f9248b9270c",
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }

        $.getJSON(settings, function (data) {

            data = data.restaurants;
            var html = "";
            var url = "about";
            $.each(data, function (index, value) {
                var restaurantData = data[index];
                $.each(restaurantData, function (index, value) {
                    var location = restaurantData.restaurant.location;
                    var userRating = restaurantData.restaurant.user_rating;
                    html += "<div class='data img-rounded'>";
                    html += "<div class='rating'>";

                    html += "<span title='" + userRating.rating_text +
                        "'><p style='color:white;background-color:#" + userRating.rating_color +
                        ";border-radius:4px;border:none;padding:2px 10px 2px 10px;text-align: center;text-decoration:none;display:inline-block;font-size:16px;float:right;'><strong>" +
                        userRating.aggregate_rating + "</strong></p></span><br>";
                    html += "  <strong class='text-info'>" + userRating.votes + " votes</strong>";
                    html += "</div>";
                    html += "<img class='resimg img-rounded' src=" + value.thumb +
                        " alt='Restaurant Image' height='185' width='185'>";
                    html += "<h2 style='color:red;'><strong>" + value.name + "</strong></h2>";
                    html += "  <strong class='text-primary'>" + location.locality + "</strong><br>";
                    html += "  <h6 style='color:grey;'><strong>" + location.address +
                        "</strong></h6><hr>";
                    html += "  <strong>CUISINES</strong>: " + value.cuisines + "<br>";
                    html += "  <strong>COST FOR TWO</strong>: " + value.currency + value
                        .average_cost_for_two + "<br>";
                    html += "<a href=" + url +
                        " ><p ><input type='submit' value='BOOk Table'style='color:white;background-color:green;width:100px;height:50px;margin-left:500px;' ></p></a>";

                    html += "</div><br>";
                });
            });
            $(".message").html(html);
        });

    }
    // query for City selection-----------------

});